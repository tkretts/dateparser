""" Parse date from file


    @author: as.bogunkov
"""
from datetime import datetime


class DateParser:
    """ Date parser class """
    _formats = [
        '%y/%m/%d',
        '%y/%d/%m',
        '%Y/%d/%m',
        '%Y/%m/%d',
        '%m/%d/%y',
        '%m/%d/%Y',
        '%m/%y/%d',
        '%m/%Y/%d',
        '%d/%m/%y',
        '%d/%m/%Y',
        '%d/%y/%m',
        '%d/%Y/%m',
    ]

    def parse(self, date: str) -> str:
        """ Parse data

            :param date: defined date from file
            :return: returns parsed date
        """
        date = date.strip()
        # Get all parts of data and filter them by rules
        date_arr = ['{:0>2}'.format(x.strip()) for x in date.split('/')
                    if str(x).strip().isdigit() and 0 <= int(x) <= 2999]
        # Check if data is correct
        if len(date_arr) != 3:
            return '{} is illegal'.format(date)

        # Try to find format
        parsed = None
        for fmt in self._formats:
            try:
                new = datetime.strptime('/'.join(date_arr), fmt)
            except ValueError:
                pass
            else:
                if not isinstance(parsed, datetime) or parsed > new:
                    parsed = new

        return parsed.strftime('%Y-%m-%d') if isinstance(parsed, datetime) else '{} is illegal'.format(date)


def process_file(filename: str) -> list:
    """ Process defined file

        :param filename: path to file
        :return: returns parsed dates
    """
    data = None
    try:
        with open(filename, 'r') as file:
            data = file.readlines()
    except FileNotFoundError:
        print('File "{}" was not found. Please check if it is exists.'.format(filename))
        exit(1)

    date_parser = DateParser()

    return [date_parser.parse(date) for date in data]


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    result = process_file(args.filename)

    print('\n'.join(result))
